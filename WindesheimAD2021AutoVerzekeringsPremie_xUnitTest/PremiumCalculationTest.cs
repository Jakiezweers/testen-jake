﻿using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using static WindesheimAD2021AutoVerzekeringsPremie.Implementation.PremiumCalculation;
using System;
using Moq;

namespace WindesheimAD2021AutoVerzekeringsPremie_xUnitTest
{
    public class PremiumCalculationTest
    {
        [Fact]
        public void isCalculateBasePremiumCorrect()
        {
            // Create a vehicle
            Vehicle testVehicle = new Vehicle(160, 4500, 1980);

            //Calculate base price
            double ActualPremiumCalculation = CalculateBasePremium(testVehicle);

            // Expected Outcome
            double expectedOutcome = 12;

            // Compare
            Assert.Equal(expectedOutcome, ActualPremiumCalculation);
        }


        [Theory]
        [InlineData("16-06-2000", 22, 14.95)]
        [InlineData("16-06-2017", 23, 14.95)]
        [InlineData("16-06-2017", 30, 14.95)]
        [InlineData("16-06-2000", 23, 13)]
        [InlineData("16-06-2016", 23, 13)]
        [InlineData("16-06-2016", 30, 13)]
        public void DoesPremiumApplyWhenPersonHasObtainedDrivingLicenseMoreThanThreeYearsAgoOrDriversAgeLessThenTwentyFiveYearsAgo(string DriversLicenseStartDate, int Age, double ExpectedOutcome)
        {
            // Create vehicle
            Vehicle testVehicle = new Vehicle(225, 3000, 1985);

            // Create PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(Age, DriversLicenseStartDate, 9999, 0);

            // Create a PremiumCalculation Based on Vehicle Policy And InsuranceCoverage
            PremiumCalculation premiumCalculation = new(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Get separate variable for the comparison
            double actualPremiumCalculation = premiumCalculation.PremiumAmountPerYear;

            // Compare
            Assert.Equal(ExpectedOutcome, actualPremiumCalculation);
        }

        [Fact]
        public void IsAllRiskTwoTimesMoreExpensive()
        {
            // Create vehicle
            Vehicle testVehicle = new Vehicle(225, 3000, 1985);

            // Create PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(25, "16-06-2015", 9999, 0);

            // Create a PremiumCalculation As WA
            PremiumCalculation premiumCalculationWA = new(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Create a PremiumCalculation As All Risk
            PremiumCalculation premiumCalculationAllRisk = new(testVehicle, testPolicyHolder, InsuranceCoverage.ALL_RISK);

            // Get separate variable for the comparison
            double AllRiskDividedByTwo = (premiumCalculationAllRisk.PremiumAmountPerYear / 200) * 100;

            // Compare
            Assert.Equal(premiumCalculationWA.PremiumAmountPerYear, AllRiskDividedByTwo);
        }

        [Fact]
        public void IsWaPlusTwentePercentageMoreExpensive()
        {
            // Create vehicle
            Vehicle testVehicle = new Vehicle(225, 3000, 1985);

            // Create PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(25, "16-06-2015", 9999, 0);

            // Create a PremiumCalculation As WA
            PremiumCalculation premiumCalculationWA = new(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Create a PremiumCalculation As All Risk
            PremiumCalculation premiumCalculationAllRisk = new(testVehicle, testPolicyHolder, InsuranceCoverage.WA_PLUS);

            // Get separate variable for the comparison
            double AllRiskDividedByTwo = (premiumCalculationAllRisk.PremiumAmountPerYear / 120) * 100;

            // Compare
            Assert.Equal(premiumCalculationWA.PremiumAmountPerYear, AllRiskDividedByTwo);
        }

        [Theory]
        [InlineData(-1, 14.95)]
        [InlineData(0, 14.95)]
        [InlineData(1, 14.95)]
        [InlineData(6, 14.2)]
        [InlineData(11, 10.46)]
        [InlineData(12, 9.72)]
        [InlineData(65, 5.23)]
        public void DoesDiscountUpdateWhenPersonHasMoreThenFiveNoClaimYears(int years, double ExpectedOutcome)
        {
            // Create vehicle
            Vehicle testVehicle = new Vehicle(225, 3000, 1985);

            // Create PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(25, "16-06-2021", 9999, years);

            // Create a PremiumCalculation
            PremiumCalculation Outcome = new(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Compare And round the Outcome to 2 decimals
            Assert.Equal(ExpectedOutcome, Math.Round(Outcome.PremiumAmountPerYear, 2));
        }

        [Theory]
        [InlineData(2000, 15.70)]
        [InlineData(4000, 15.25)]
        [InlineData(8000, 14.95)]
        public void DoesPostalCodeChangePremiumPrice(int postalcode, double ExpectedOutcome)
        {
            // Create vehicle
            Vehicle testVehicle = new Vehicle(225, 3000, 1985);

            // Create PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(25, "16-06-2021", postalcode, 0);

            // Create a PremiumCalculation
            PremiumCalculation Outcome = new(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Compare And round the Outcome to 2 decimals
            Assert.Equal(ExpectedOutcome, Math.Round(Outcome.PremiumAmountPerYear, 2));
        }

        [Fact]
        public void DoesPremiumPaymentAmountYearlyReturnTwoPointFivePercentDiscount()
        {
            // Create vehicle
            Vehicle testVehicle = new Vehicle(225, 3000, 1985);

            // Create PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(25, "16-06-2021", 9999, 0);

            // Create a PremiumCalculation Mock
            Mock<PremiumCalculation> PremiumCalculationMock = new(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            //Setup the PremiumCalculation Mock
            PremiumCalculationMock.SetupAllProperties();
            PremiumCalculationMock.SetupGet(p => p.PremiumAmountPerYear).Returns(1000);

            // Compare
            Assert.Equal(975, PremiumCalculationMock.Object.PremiumPaymentAmount(PaymentPeriod.YEAR));
        }

        [Fact]
        public void DoesPremiumPaymentAmountMonthlyReturnCorrectValue()
        {
            // Create vehicle
            Vehicle testVehicle = new Vehicle(225, 3000, 1985);

            // Create PolicyHolder
            PolicyHolder testPolicyHolder = new PolicyHolder(25, "16-06-2021", 9999, 0);

            // Create a PremiumCalculation
            PremiumCalculation Outcome = new(testVehicle, testPolicyHolder, InsuranceCoverage.WA);

            // Compare the outcome
            Assert.Equal(1.25, Outcome.PremiumPaymentAmount(PaymentPeriod.MONTH));
        }
    }
}

