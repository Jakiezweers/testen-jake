using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremie_xUnitTest
{
    public class PolicyHolderTest
    {
        [Theory]
        [InlineData("15-06-1996", 25)]
        [InlineData("15-06-2021", 0)]
        [InlineData("15-06-2022", 0)]
        public void IsDriverLicenseStartDateAsStartDate(string StartDate, int ExpectedAge)
        {
            //Create Policy Holder
            PolicyHolder policyHolder = new PolicyHolder(30, StartDate, 9999, 0);

            //Fetch License Age
            int ActualDriverLicenseStartDate = policyHolder.LicenseAge;

            // Check if the age Driver license Start date is as expected
            Assert.Equal(ExpectedAge, ActualDriverLicenseStartDate);
        }
    }
}
