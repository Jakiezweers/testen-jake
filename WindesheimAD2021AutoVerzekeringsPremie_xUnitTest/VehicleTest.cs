using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremie_xUnitTest
{
    public class VehicleTest
    {
        [Theory]
        [InlineData(1981, 40)]
        [InlineData(2021, 0)]
        [InlineData(2050, 0)]
        public void isVehicleAgeBelowZero(int ConstructionYear, int ExpectedOutcome)
        {
            //Create vehicle
            Vehicle vehicle = new Vehicle(235, 3000, ConstructionYear);

            //Get vehicle Age
            int Age = vehicle.Age;

            //Check if the outcome is not below zero
            Assert.Equal(ExpectedOutcome, Age);
        }
    }
}
