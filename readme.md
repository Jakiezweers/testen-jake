# Jake Zweers - Testen 
In deze readme zal ik onderbouwen welke testen er zijn gemaakt.
# Basis calculatie (Premie)
Met deze test wil ik kijken of de Basis premie goed berekent wordt.
### Technieken
- [Fact]
### Dataset
Er wordt met een auto getest met de waardes:
- KW: 160
- Value: 4500
- ConstructionYear: 1980
### Tested
Of de formule wel gelijk is aan de verwachtte waardes.
### Verwachting
Dat het resultaat 12 is
### Result
Ik krijg hier 14 uit. Dit is niet kloppend met het resultaat. Dit is gekomen doordat de formule: 
 **Waarde voertuig / 100 - leeftijd + vermogen in KW / 5 / 3** 
 is terwijl deze:
 **(Waarde voertuig / 100 - leeftijd + vermogen in KW / 5) / 3** had moeten zijn
 
 
 # Postcode verandert premium prijs
Hiermee wordt de opslag gecheckt op basis van de postcode
### Technieken
- [Theory]
### Dataset
Voor de data hebben we getest met de volgende postcodes
- 2000
- 4000
- 9000

en met de volgende informatie

Vehicle
- 225PKW
- 3000 Euro
- 1985 Bouwjaar

Policy
- 25 Jaar
- `16-06-2021` Rijbewijs start datum
- 0 No Claim Years

### Tested
Of op basis van de postcode er een risico opslag erbij wordt op gerekend
### Verwachting & Result

|Postcode|Verwachting|Result|
|----------------|-------------------------------|-----------------------------|
|2000|15.70|15.70|
|4000|15.25|15.25|
|9000|14.95|14.95|

Zoals hierboven al te zien is, Is de opslag voor de postcode berekening correct



 # Korting wordt gegeven na 5 jaar no claim
Met deze test wordt er gekeken of er na 5 jaar no claim een korting wordt gegeven.
### Technieken
- [Theory]
### Dataset
Voor de data hebben we getest met verschillende no claim years (-1, 0, 1, 6, 11, 12, 65)
### Tested
Of er korting wordt gegeven gebaseerd op het aantal no-claim years ( Voor het testen ronden we de cijfers af naar 2 decimals. )
### Verwachting
|No-claim Years|Verwachting|
|----------------|-------------------------------|
|-1|14.95|
|0|14.95|
|1|14.95|
|6|14.2|
|11|10.46|
|12|9.72|
|65|5.23|
### Result
Hierbij werdt geconstanteerd dat in de functie een berekening werdt gedaan
> `int NoClaimPercentage = (years - 5) * 5;`

Dit had

> `double NoClaimPercentage = (years - 5) * 5;`

Moeten zijn

 # 15% premie-opslag wanneer leeftijd onder 23 jaar of rijbewijs korter dan 5 jaar
Met deze test wordt er gekeken of er na 5 jaar no claim een korting wordt gegeven.
### Technieken
- [Theory]
### Dataset
|#|Start datum Rijbewijs|Leeftijd|
|----|----------------|---------|
|1|`16-06-2000`|22|
|2|`16-06-2017`|23|
|3|`16-06-2017`|30|
|4|`16-06-2000`|23|
|5|`16-06-2016`|23|
|6|`16-06-2016`|30|
### Tested
Of er een premie opslag van 15% wordt opgezet gebaseerd op:
- onder 23 jaar
- Rijbewijs korter dan 5 jaar
### Verwachting
|#|Verwachting|
|----------------|----------------|
|1|14.95|
|2|14.95|
|3|14.95|
|4|13|
|5|13|
|6|13|

### Result

De leeftijd geeft geen problemen aan, Maar bij de Rijbewijs korter dan 5 jaar was gebleken, Dat de check op:

> ` policyHolder.LicenseAge <= 5`

gezet was, Dit had 

> ` policyHolder.LicenseAge < 5`

Moeten zijn

 # Jaarlijkse betaling geeft 2,5% korting
Met deze test wordt er gekeken of bij een jaarlijkse betaling een korting van 2,5% wordt gegeven

### Technieken
- [Fact]

### Dataset
We hebben voor deze test met de volgende informatie getest:

Vehicle
- 225PKW
- 3000 Euro
- 1985 Bouwjaar

Policy
- 25 Jaar
- `16-06-2021` Rijbewijs start datum
- `9999` Postcode
- 0 No Claim Years

### Tested
Of er een korting van 2,5% gegeven wordt wanneer er een jaarlijkse betaling van gemaakt wordt.

### Verwachting
De er een discount van 2,5 procent wordt gegeven.

### Result
Er werdt een berekening gedaan, Helaas gaf deze niet een 2,5% korting. Dit is gekomen door:
> `PremiumAmountPerYear / 1.025`

Dit had
> `PremiumAmountPerYear * 0.975`

Moeten zijn.

# Other Tests
Hier zijn een paar andere tests die ondersteuning geven voor de rest van de code.

### Vehicle leeftijd onder 0
> [Theory]

### Maandelijkse betaling correcte value returned
> [Fact]

### Rijbewijs datum als een start datum
> [Theory]

### All Risk 2 keer zo duur
> [Fact]

### WA plus 25% duurder
> [Fact]